﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AVSWebMvc.Models
{
    public class ResultModel
    {
        public string YEAR { get; set; }
        public string RegNo { get; set; }
        public string NAME { get; set; }
        public string CLASS { get; set; }
        public decimal? ATTENDANCE { get; set; }
        public decimal? Tamil { get; set; }
        public decimal? English { get; set; }
        public decimal? Maths { get; set; }

        public decimal? Science { get; set; }
        public decimal? Social { get; set; }
        public decimal? Total { get; set; }
        public decimal? Average { get; set; }
        public string Result { get; set; }


        // Grade
        public string GTamil { get; set; }
        public string GEnglish { get; set; }
        public string GMaths { get; set; }
        public string GScience { get; set; }
        public string GSocial { get; set; }

        public string ResultDate { get; set; }
    }
}