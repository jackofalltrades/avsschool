﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvsSchoolMvc.Service
{
    public class UserAuth : System.Web.Mvc.Controller
    {
        public bool checkUserSession(HttpSessionStateBase objSes)
        {
            if (objSes["user"] != null)
            {
                if (objSes["user"].ToString() == "AVS")
                {
                    return true;
                }
            }
            
            TempData["msg"] = "<script>alert('Invalid Credentials');</script>";
            return false;
        }
    }
}