﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using System.Data;
using AvsSchoolMvc.Models;
using AvsSchoolMvc.Models.DB;
namespace AvsSchoolMvc.Controllers
{
    public class HomeController : Controller
    {
        private AVSSchoolEntities db = new AVSSchoolEntities();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            ViewBag.ALink = "Home";

            var ms_news = db.MS_NEWS.Where(s => s.RES_STATUS == "A").Take(3);
            foreach (var itm in ms_news)
            {
                itm.NEWS_IMAGE = "/Files/News/" + itm.NEWS_IMAGE;
                

            }
            ViewBag.data = ms_news;
            var ms_gallary = db.MS_GALLARY.Where(s => s.RES_STATUS == "A").Take(3);
            foreach (var itm in ms_gallary)
            {
                itm.GAL_IMAGE = "/Files/Gallary/" + itm.GAL_IMAGE;
            }
            ViewBag.galdata = ms_gallary;
            var ms_school_topper = db.MS_SCHOOL_TOPPER.Where(s => s.RES_STATUS == "A").OrderByDescending(s=>s.SCL_TOP_ID).Take(3);
            
            ViewBag.topperdata = ms_school_topper;
            return View();
            
        }        

        public ActionResult getNewsList()
        {
            var ms_news = db.MS_NEWS.Where(s => s.RES_STATUS == "A");
            foreach (var itm in ms_news)
            {
                itm.NEWS_IMAGE = "/Files/News/" + itm.NEWS_IMAGE;
            }
            ViewBag.data = ms_news;

            return View();

        }
        public ActionResult getGallaryList()
        {
            var ms_gallary = db.MS_GALLARY.Where(s => s.RES_STATUS == "A");
            foreach (var itm in ms_gallary)
            {
                itm.GAL_IMAGE = "/Files/Gallary/" + itm.GAL_IMAGE;
            }
            ViewBag.galdata = ms_gallary;

            return View();

        }
        public ActionResult getTopperList()
        {
            var ms_Topper = db.MS_SCHOOL_TOPPER.Where(s => s.RES_STATUS == "A").OrderByDescending(s=>s.SCL_TOP_ID);
            ViewBag.Topperdata = ms_Topper;

            return View();

        }
        public ActionResult getFacilityist()
        {
           
            return View();

        }

        public ActionResult AdminPortal()
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS") 
                {
                    return View();
                }
                else
                {
                    TempData["msg"] = "<script>alert('Invalid Credentials');</script>";

                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["msg"] = "<script>alert('Invalid Credentials');</script>";

                return RedirectToAction("Index");
            }
        }

        public ActionResult Result()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(HttpPostedFileBase file)
        {
            string uName = Request.Form[0];
            string pwd = Request.Form[1];

          
                if (uName == "AVS" && pwd == "AVS2019")
                {
                    Session["user"] = "AVS";
                    
                    return RedirectToAction("AdminPortal");
                }
                else
                {
                    TempData["msg"] = "<script>alert('Invalid Credentials');</script>";

                    return RedirectToAction("Index");
                }
           
        }
        public ActionResult Logout()
        {
            Session["user"] = null;

            return RedirectToAction("Index");
           
        }


        public ActionResult GetResultDetails(int id)
        {
            var res = getResult(id);
            if (res != null)
            {
                if (res.NAME != "" && res.NAME != null)
                    return PartialView(res);
                else 
                    return null;
            }
            else
                return null;
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            //string uName = Request.Form[0];
            //string pwd = Request.Form[1];

            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS") // (uName == "AVS" && pwd == "AVS2019")
                {
                    //Session["user"] = "AVS";
                    if (file.ContentLength > 0)
                    {
                        var fileName = "Result.xlsx"; // Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Result"), fileName);
                        file.SaveAs(path);
                    }
                    TempData["msg"] = "<script>alert('Uploaded Successfully');</script>";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["msg"] = "<script>alert('Invalid Credentials');</script>";

                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["msg"] = "<script>alert('Invalid Credentials');</script>";

                return RedirectToAction("Index");
            }
        }

        private ResultModel getResult(int id)
        {
            string resDate = ConfigurationManager.AppSettings["ResDate"].ToString();
            string resYear = ConfigurationManager.AppSettings["ResYear"].ToString();
            System.Data.OleDb.OleDbConnection MyConnection;
            string conn = string.Format(ConfigurationManager.ConnectionStrings["dbconnection"].ToString(), HttpContext.Server.MapPath("~/Result"));
            MyConnection = new System.Data.OleDb.OleDbConnection(conn);
            MyConnection.Open();
            System.Data.OleDb.OleDbDataAdapter myCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [MainResults$] where [RegNo] = "+id, MyConnection);
            DataTable dt = new DataTable();            
            myCommand.Fill(dt);

            
            ResultModel objMOd = new ResultModel();
            objMOd.ResultDate = resDate;
            objMOd.YEAR = resYear;
            if (dt.Rows.Count > 0)
            {
                objMOd.RegNo = dt.Rows[0]["RegNo"].ToString();
                objMOd.NAME = dt.Rows[0]["NAME"].ToString();
                objMOd.ATTENDANCE = dt.Rows[0]["Attendance"].ToString() != "" ? Convert.ToDecimal(dt.Rows[0]["Attendance"].ToString()) : 0;

                objMOd.Maths = dt.Rows[0]["Maths"].ToString() != "" ? Convert.ToDecimal(dt.Rows[0]["Maths"].ToString()) : 0;
                objMOd.GMaths = getGrade(objMOd.Maths);

                objMOd.Tamil = dt.Rows[0]["Tamil"].ToString() != "" ? Convert.ToDecimal(dt.Rows[0]["Tamil"].ToString()) : 0;
                objMOd.GTamil = getGrade(objMOd.Tamil);

                objMOd.Science = dt.Rows[0]["Science"].ToString() != "" ? Convert.ToDecimal(dt.Rows[0]["Science"].ToString()) : 0;
                objMOd.GScience = getGrade(objMOd.Science);

                objMOd.Social = dt.Rows[0]["Social"].ToString() != "" ? Convert.ToDecimal(dt.Rows[0]["Social"].ToString()) : 0;
                objMOd.GSocial = getGrade(objMOd.Social);

                objMOd.English = dt.Rows[0]["English"].ToString() != "" ? Convert.ToDecimal(dt.Rows[0]["English"].ToString()) : 0;
                objMOd.GEnglish = getGrade(objMOd.English);

                objMOd.Result = dt.Rows[0]["Result"].ToString();
                
                objMOd.CLASS = dt.Rows[0]["CLASS"].ToString();
            }

            return objMOd;
        }

        private string getGrade(decimal? val)
        {
            string grade = "E2";
            if (val > 90)
                grade = "A1";
            else if (val > 80)
                grade = "A2";
            else if (val > 70)
                grade = "B1";
            else if (val > 60)
                grade = "B2";

            else if (val > 50)
                grade = "C1";
            else if (val > 40)
                grade = "C2";
            else if (val > 32)
                grade = "D";
            else if (val > 20)
                grade = "E1";
            else //if (val > 0)
                grade = "E2";

            return grade;
        }

    }    
}
