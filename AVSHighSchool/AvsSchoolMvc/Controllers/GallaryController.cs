﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using AvsSchoolMvc.Models.DB;
using AvsSchoolMvc.Service;

namespace AvsSchoolMvc.Controllers
{
    public class GallaryController : Controller
    {
        private AVSSchoolEntities db = new AVSSchoolEntities();
        private UserAuth objAUth = new UserAuth();

        //
        // GET: /Gallary/

        public ActionResult Index()
        {
            try
            {
                if (objAUth.checkUserSession(Session))
                {
                    var ms_gallary = db.MS_GALLARY.Where(s => s.RES_STATUS == "A");
                    foreach (var itm in ms_gallary)
                    {
                        itm.GAL_IMAGE = "/Files/Gallary/" + itm.GAL_IMAGE;
                    }
                    return View(ms_gallary.ToList());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //
        // GET: /Gallary/Details/5

        public ActionResult Details(int id)
        {
            MS_GALLARY ms_gallary = db.MS_GALLARY.Single(m => m.GAL_ID == id);
            ms_gallary.GAL_IMAGE = "/Files/Gallary/" + ms_gallary.GAL_IMAGE;
            if (ms_gallary == null)
            {
                return HttpNotFound();
            }
            return View(ms_gallary);
        }

        //
        // GET: /Gallary/Create

        public ActionResult Create()
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /Gallary/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MS_GALLARY ms_gallary, HttpPostedFileBase ImageFile)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                if (ModelState.IsValid)
                {
                    ms_gallary.GAL_ID = db.MS_GALLARY.Any() ? (db.MS_GALLARY.Max(S => S.GAL_ID)) + 1 : 1;
                    string filename = Path.GetFileNameWithoutExtension(ImageFile.FileName);
                    string extension = Path.GetExtension(ImageFile.FileName);
                    filename = filename + extension;
                    filename = "GAL" + ms_gallary.GAL_ID + extension;
                    ms_gallary.GAL_IMAGE = filename;
                    filename = Path.Combine(Server.MapPath("~/Files/Gallary/"), filename);
                    ImageFile.SaveAs(filename);
                    ms_gallary.RES_STATUS = "A";
                    db.MS_GALLARY.Add(ms_gallary);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            return View(ms_gallary);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /Gallary/Edit/5

        public ActionResult Edit(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                MS_GALLARY ms_gallary = db.MS_GALLARY.Single(m => m.GAL_ID == id);
                ms_gallary.GAL_IMAGE = "/Files/Gallary/" + ms_gallary.GAL_IMAGE;
                if (ms_gallary == null)
            {
                return HttpNotFound();
            }
            return View(ms_gallary);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /Gallary/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MS_GALLARY ms_gallary, HttpPostedFileBase ImageFile)
        {
            MS_GALLARY us_mast_gallary = db.MS_GALLARY.FirstOrDefault(s => s.GAL_ID == ms_gallary.GAL_ID);
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                if (ModelState.IsValid)
            {
                    if (ImageFile != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(ImageFile.FileName);
                        string extension = Path.GetExtension(ImageFile.FileName);
                        filename = filename + extension;
                        filename = "GAL" + ms_gallary.GAL_ID + extension;
                        ms_gallary.GAL_IMAGE = filename;
                        filename = Path.Combine(Server.MapPath("~/Files/Gallary/"), filename);
                        ImageFile.SaveAs(filename);
                        us_mast_gallary.GAL_IMAGE = ms_gallary.GAL_IMAGE;
                    }
                     us_mast_gallary.DESCRIP = ms_gallary.DESCRIP;
                    
                    //db.MS_GALLARY.Attach(ms_gallary);
                //db.ObjectStateManager.ChangeObjectState(ms_gallary, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ms_gallary);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /Gallary/Delete/5

        public ActionResult Delete(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                MS_GALLARY ms_gallary = db.MS_GALLARY.Single(m => m.GAL_ID == id);
                ms_gallary.GAL_IMAGE = "/Files/Gallary/" + ms_gallary.GAL_IMAGE;
                if (ms_gallary == null)
            {
                return HttpNotFound();
            }
            return View(ms_gallary);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /Gallary/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                MS_GALLARY ms_gallary = db.MS_GALLARY.Single(m => m.GAL_ID == id);
                ms_gallary.RES_STATUS = "D";
                // db.MS_GALLARY.DeleteObject(ms_gallary);
                db.SaveChanges();
            return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}