﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using AvsSchoolMvc.Models.DB;
using AvsSchoolMvc.Service;

namespace AvsSchoolMvc.Controllers
{
    public class NewsController : Controller
    {
        private AVSSchoolEntities db = new AVSSchoolEntities();
        private UserAuth objAUth = new UserAuth();

        //
        // GET: /News/

        public ActionResult Index()
        {
            try
            {
                if (objAUth.checkUserSession(Session))
                {
                    var ms_news = db.MS_NEWS.Where(s => s.RES_STATUS == "A");
                    foreach (var itm in ms_news)
                    {
                        itm.NEWS_IMAGE = "/Files/News/" + itm.NEWS_IMAGE;
                    }
                    return View(ms_news.ToList());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //
        // GET: /News/Details/5

        public ActionResult Details(int id)
        {
            try
            {
                if (objAUth.checkUserSession(Session))
                {
                    MS_NEWS ms_news = db.MS_NEWS.Single(m => m.NEWS_ID == id);
                    ms_news.NEWS_IMAGE = "/Files/News/" + ms_news.NEWS_IMAGE;
                    if (ms_news == null)
                    {
                        return HttpNotFound();
                    }
                    return View(ms_news);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //
        // GET: /News/Create

        public ActionResult Create()
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /News/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MS_NEWS ms_news, HttpPostedFileBase ImageFile)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                if (ModelState.IsValid)
            {
                    ms_news.NEWS_ID = db.MS_NEWS.Any() ? (db.MS_NEWS.Max(S => S.NEWS_ID)) + 1 : 1;
                    string filename = Path.GetFileNameWithoutExtension(ImageFile.FileName);
                    string extension = Path.GetExtension(ImageFile.FileName);
                    filename = filename + extension;
                    filename = "NEWS" + ms_news.NEWS_ID + extension;
                    ms_news.NEWS_IMAGE = filename;
                    filename = Path.Combine(Server.MapPath("~/Files/News/"), filename);
                    ImageFile.SaveAs(filename);
                    ms_news.RES_STATUS = "A";
                    db.MS_NEWS.Add(ms_news);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ms_news);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /News/Edit/5

        public ActionResult Edit(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                MS_NEWS ms_news = db.MS_NEWS.Single(m => m.NEWS_ID == id);
                ms_news.NEWS_IMAGE = "/Files/News/" + ms_news.NEWS_IMAGE;
                if (ms_news == null)
            {
                return HttpNotFound();
            }
            return View(ms_news);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /News/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MS_NEWS ms_news, HttpPostedFileBase ImageFile)
        {
            MS_NEWS us_mast_news = db.MS_NEWS.FirstOrDefault(s=>s.NEWS_ID == ms_news.NEWS_ID);
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                
                if (ModelState.IsValid)
            {
                    
                    if (ImageFile != null)
                    {
                        string filename = Path.GetFileNameWithoutExtension(ImageFile.FileName);
                        string extension = Path.GetExtension(ImageFile.FileName);
                        filename = filename + extension;
                        filename = "NEWS" + ms_news.NEWS_ID + extension;
                        ms_news.NEWS_IMAGE = filename;
                        filename = Path.Combine(Server.MapPath("~/Files/News/"), filename);
                        ImageFile.SaveAs(filename);
                        us_mast_news.NEWS_IMAGE = ms_news.NEWS_IMAGE;
                    }
                    //db.MS_NEWS.Attach(ms_news);
                    us_mast_news.TITLE = ms_news.TITLE;
                    us_mast_news.NEWS_DATE = ms_news.NEWS_DATE;
                    us_mast_news.DESCRIP = ms_news.DESCRIP;

                    db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ms_news);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /News/Delete/5

        public ActionResult Delete(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                MS_NEWS ms_news = db.MS_NEWS.Single(m => m.NEWS_ID == id);
                ms_news.NEWS_IMAGE = "/Files/News/" + ms_news.NEWS_IMAGE;
                if (ms_news == null)
            {
                return HttpNotFound();
            }
            return View(ms_news);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /News/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                MS_NEWS ms_news = db.MS_NEWS.Single(m => m.NEWS_ID == id);
            ms_news.RES_STATUS = "D";
            //db.MS_NEWS.DeleteObject(ms_news);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}