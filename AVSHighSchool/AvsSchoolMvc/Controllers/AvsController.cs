﻿using AvsSchoolMvc.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvsSchoolMvc.Controllers
{
    public class AvsController : Controller
    {
        private AVSSchoolEntities db = new AVSSchoolEntities();

        // GET: Avs
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewsList()
        {
            var ms_news = db.MS_NEWS.Where(s => s.RES_STATUS == "A");
            foreach (var itm in ms_news)
            {
                itm.NEWS_IMAGE = "/Files/News/" + itm.NEWS_IMAGE;
            }
            ViewBag.data = ms_news;

            return View();

        }
        public ActionResult GalleryList()
        {
            var ms_gallary = db.MS_GALLARY.Where(s => s.RES_STATUS == "A");
            foreach (var itm in ms_gallary)
            {
                itm.GAL_IMAGE = "/Files/Gallary/" + itm.GAL_IMAGE;
            }
            ViewBag.galdata = ms_gallary;

            return View();

        }
        public ActionResult TopperList()
        {
            var ms_Topper = db.MS_SCHOOL_TOPPER.Where(s => s.RES_STATUS == "A").OrderByDescending(s => s.SCL_TOP_ID);
            ViewBag.Topperdata = ms_Topper;

            return View();

        }
        public ActionResult Facilityist()
        {

            return View();

        }

        public ActionResult AboutUs()
        {
            ViewBag.ALink = "About";
            return View();
        }

        public ActionResult ApplyForAdmission()
        {
            ViewBag.ALink = "Apply";
            return View();
        }

        public ActionResult Gallery()
        {
            ViewBag.ALink = "Press";
            return View();
        }

        public ActionResult News()
        {
            ViewBag.ALink = "Press";
            return View();
        }

        public ActionResult Academics()
        {
            ViewBag.ALink = "Academic";
            return View();
        }

        public ActionResult Achievements()
        {
            ViewBag.ALink = "Academic";

            var ms_Topper = db.MS_SCHOOL_TOPPER.Where(s => s.RES_STATUS == "A").OrderByDescending(s => s.SCL_TOP_ID);
            ViewBag.Topperdata = ms_Topper;
            
            return View();
        }

        public ActionResult Facilities()
        {
            ViewBag.ALink = "Academic";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.ALink = "Contact";
            return View();
        }
    }
}