﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using AVSMvc.Models.DB;

namespace AVSMvc.Controllers
{
    public class SchoolTopperController : Controller
    {
        private AVSSchoolEntities db = new AVSSchoolEntities();

        //
        // GET: /SchoolTopper/

        public ActionResult Index()
        {
            try
            {
                var ms_school_topper = db.MS_SCHOOL_TOPPER.Where(s=>s.RES_STATUS == "A").OrderByDescending(s=>s.SCL_TOP_ID);
                return View(ms_school_topper.ToList());
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //
        // GET: /SchoolTopper/Details/5

        public ActionResult Details(int id)
        {

            MS_SCHOOL_TOPPER ms_school_topper = db.MS_SCHOOL_TOPPER.Single(m => m.SCL_TOP_ID == id);
            if (ms_school_topper == null)
            {
                return HttpNotFound();
            }
            return View(ms_school_topper);
        }

        //
        // GET: /SchoolTopper/Create

        public ActionResult Create()
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /SchoolTopper/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MS_SCHOOL_TOPPER ms_school_topper)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                if (ModelState.IsValid)
            {
                    ms_school_topper.SCL_TOP_ID = db.MS_SCHOOL_TOPPER.Any() ? (db.MS_SCHOOL_TOPPER.Max(S => S.SCL_TOP_ID)) + 1 : 1;
                    ms_school_topper.RES_STATUS = "A";
                    db.MS_SCHOOL_TOPPER.Add(ms_school_topper);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ms_school_topper);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /SchoolTopper/Edit/5

        public ActionResult Edit(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                    MS_SCHOOL_TOPPER ms_school_topper = db.MS_SCHOOL_TOPPER.Single(m => m.SCL_TOP_ID == id);
                if (ms_school_topper == null)
            {
                return HttpNotFound();
            }
            return View(ms_school_topper);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /SchoolTopper/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MS_SCHOOL_TOPPER ms_school_topper)
        {
            MS_SCHOOL_TOPPER us_mast_topper = db.MS_SCHOOL_TOPPER.FirstOrDefault(s=>s.SCL_TOP_ID == ms_school_topper.SCL_TOP_ID);
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                
                if (ModelState.IsValid)
            {      
                        us_mast_topper.NAME = ms_school_topper.NAME;
                        us_mast_topper.YEAR = ms_school_topper.YEAR;
                        us_mast_topper.MARKS = ms_school_topper.MARKS;

                    db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ms_school_topper);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /SchoolTopper/Delete/5

        public ActionResult Delete(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                    MS_SCHOOL_TOPPER ms_school_topper = db.MS_SCHOOL_TOPPER.Single(m => m.SCL_TOP_ID == id);
                if (ms_school_topper == null)
            {
                return HttpNotFound();
            }
            return View(ms_school_topper);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /SchoolTopper/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["user"] != null)
            {
                if (Session["user"].ToString() == "AVS")
            {
                    MS_SCHOOL_TOPPER ms_school_topper = db.MS_SCHOOL_TOPPER.Single(m => m.SCL_TOP_ID == id);
                    ms_school_topper.RES_STATUS = "D";
            //db.MS_NEWS.DeleteObject(ms_news);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}