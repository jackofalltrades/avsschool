CREATE TABLE MS_NEWS (
    [NEWS_ID]    INT            NOT NULL,
    [NEWS_DATE]  DATETIME       NULL,
    [TITLE]      NVARCHAR (100) NULL,
    [DESCRIP]    NVARCHAR (MAX) NULL,
    [RES_STATUS] NVARCHAR (5)   NULL,
    [NEWS_IMAGE] NVARCHAR (200) NULL,
    PRIMARY KEY CLUSTERED ([NEWS_ID] ASC)
);